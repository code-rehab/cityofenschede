<?php
/*
Plugin Name: Student News
Description: Student News Items
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_studentnews() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'Student news',
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) );
    register_post_type( 'studentnewsitems', $args );
}
function icl_theme_register_custom_studentnews() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_studentnews','icl_theme_register_custom_studentnews','my_rewrite_flush_studentnews' );
function my_rewrite_flush_studentnews() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_studentnews' );

?>