<?php
/**
 * Custom Walker
 *
 * @access      public
 * @since       1.0 
 * @return      void
*/
class ik_walker extends Walker_Nav_Menu
{
      /* function start_el(&$output, $item, $depth, $args)
      {
           global $wp_query;
           $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

           $class_names = $value = '';

           $classes = empty( $item->classes ) ? array() : (array) $item->classes;

           $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
           $class_names = ' class="'. esc_attr( $class_names ) . '"';

           $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

           $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
           $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
           $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
           $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

           $prepend = '<strong>';
           $append = '</strong>';
           $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

           if($depth != 0)
           {
	           $description = $append = $prepend = "";
           }

            $item_output = $args->before;
            $item_output .= '<a'. $attributes .'>';
            $item_output .= $args->link_before .$prepend.apply_filters( 'the_title', $item->title, $item->ID ).$append;
            //$item_output .= $description.$args->link_after;
            $item_output .= ' '.$item->subtitle.'</a>';
            $item_output .= $args->after;

            $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
            }
      */
      
      //start of the sub menu wrap
  function start_lvl(&$output, $depth) {
    $output .= '<ul class="list">';
  }
 
  //end of the sub menu wrap
  function end_lvl(&$output, $depth) {
    $output .= '
          </ul>';
  } 
 
  //add the description to the menu item output
  function start_el(&$output, $item, $depth, $args) {
    global $wp_query;
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
 
    $class_names = $value = '';
 
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
 
    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
    $class_names = ' class="' . esc_attr( $class_names ) . '"';
 
    $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
 
    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
    $item_output = $args->before;
    
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
    $item_output .= '</a>';
    $item_output .= '<div class="flyout">';
    //if(strlen($item->description)>2){ 
    $item_output .= '<div class="menu-description">';
    $item_output .= '<h2>' . $item->subtitle . '</h2>'; 
    $item_output .= '<span>' . $item->subdescription . '</span>'; 
    $item_output .= '<div class="fly-img">';
    $item_output .= '<div class="color-overlay"></div>'; 
    $item_output .= '<img src="'.$item->subvisual.'" alt="'. $item->subtitle .'"/>';
    $item_output .= '</div>';
    //}
    $item_output .= '</div>';
    $output .= apply_filters( 'walker_nav_menu_start_el', 
    $item_output, $item, $depth, $args );
    $item_output .= '</div>';





    //$item_output .= $args->before;
 
  }      
}