<?php
/*
Plugin Name: Connect Footer
Description: Connect Items
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_connect() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'Connect',
    	'exclude_from_search' => true,
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) 
    	); 
    	
    register_post_type( 'connect', $args );
}
function icl_theme_register_custom_connect() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_connect','icl_theme_register_custom_connect','my_rewrite_flush_connect' );
function my_rewrite_flush_connect() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_connect' );

?>