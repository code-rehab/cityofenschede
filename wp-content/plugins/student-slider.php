<?php
/*
Plugin Name: Student Slider
Description: Animated Student Slider
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_studentslider() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'Student slider',
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) );
    register_post_type( 'studentslider', $args );
}
function icl_theme_register_custom_studentslider() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_studentslider','icl_theme_register_custom_studentslider','my_rewrite_flush_studentslider' );
function my_rewrite_flush_studentslider() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_studentslider' );

?>