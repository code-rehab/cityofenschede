<?php
/*
Plugin Name: HighLights
Description: Global Highights
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_highlights() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'Highlights',
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) );
    register_post_type( 'highlights', $args );
}
function icl_theme_register_custom_highlights() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_highlights','icl_theme_register_custom_highlights','my_rewrite_flush_highlights' );
function my_rewrite_flush_highlights() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_highlights' );

?>