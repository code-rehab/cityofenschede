<?php

/*
Plugin Name: Wordpress Login Panel
Description: Controle the styling of your login panel
Author: Squal
Version: 1.0
*/

function login_theme_style() {
    wp_enqueue_style('login-theme', plugins_url('login.css', __FILE__));
}
add_action('admin_enqueue_scripts', 'login_theme_style');
add_action('login_enqueue_scripts', 'login_theme_style');

?>
