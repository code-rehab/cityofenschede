<?php
/*
Plugin Name: Popular
Description: Most Popular
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_mostpopular() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'Most Popular',
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) );
    register_post_type( 'mostpopular', $args );
}
function icl_theme_register_custom_mostpopular() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_mostpopular','icl_theme_register_custom_mostpopular','my_rewrite_flush_mostpopular' );
function my_rewrite_flush_mostpopular() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_mostpopular' );

?>