<?php
/*
Plugin Name: Sidebanner
Description: Side Banner Edit
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_sidebanner() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'Side Banner',
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) );
    register_post_type( 'sidebanner', $args );
}
function icl_theme_register_custom_sidebanner() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_sidebanner','icl_theme_register_custom_sidebanner','my_rewrite_flush_sidebanner' );
function my_rewrite_flush_sidebanner() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_sidebanner' );

?>