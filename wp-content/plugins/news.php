<?php
/*
Plugin Name: News
Description: News Items
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_news() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'News',
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) );
    register_post_type( 'newsitems', $args );
}
function icl_theme_register_custom_news() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_news','icl_theme_register_custom_news','my_rewrite_flush_news' );
function my_rewrite_flush_news() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_news' );

?>