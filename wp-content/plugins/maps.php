<?php
/*
Plugin Name: Maps markers
Description: Placing Markers
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_mapsmarker() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'Google maps',
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) );
    register_post_type( 'mapsmarker', $args );
}
function icl_theme_register_custom_mapsmarker() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_mapsmarker','icl_theme_register_custom_mapsmarker','my_rewrite_flush_mapsmarker' );
function my_rewrite_flush_mapsmarker() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_mapsmarker' );

?>