<?php
/*
Plugin Name: Student
Description: Student overview
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_student() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'Student life',
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) );
    register_post_type( 'student', $args );
}
function icl_theme_register_custom_student() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_student','icl_theme_register_custom_student','my_rewrite_flush_student' );
function my_rewrite_flush_student() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_student' );

?>