<?php
/*
Plugin Name: Connect Footer
Description: Connect Footer Items
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_verbinding() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'Connect Footer',
    	'exclude_from_search' => true,
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) 
    	); 
    	
    register_post_type( 'verbinding', $args );
}
function icl_theme_register_custom_verbinding() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_verbinding','icl_theme_register_custom_verbinding','my_rewrite_flush_verbinding' );
function my_rewrite_flush_verbinding() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_verbinding' );

?>