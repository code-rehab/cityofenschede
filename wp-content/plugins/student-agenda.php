<?php
/*
Plugin Name: Student Agenda
Description: Student Agenda overview
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_studentagenda() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'Student agenda',
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) );
    register_post_type( 'studentagenda', $args );
}
function icl_theme_register_custom_studentagenda() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_studentagenda','icl_theme_register_custom_studentagenda','my_rewrite_flush_studentagenda' );
function my_rewrite_flush_studentagenda() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_studentagenda' );

?>