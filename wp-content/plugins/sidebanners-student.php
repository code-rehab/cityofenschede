<?php
/*
Plugin Name: Student Sidebanner
Description: Student Side Banner Edit
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_studentsidebanner() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'Side Banner Student',
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) );
    register_post_type( 'studentsidebanner', $args );
}
function icl_theme_register_custom_studentsidebanner() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_studentsidebanner','icl_theme_register_custom_studentsidebanner','my_rewrite_flush_studentsidebanner' );
function my_rewrite_flush_studentsidebanner() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_studentsidebanner' );

?>