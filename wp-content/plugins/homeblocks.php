<?php
/*
Plugin Name: Homeblocks
Description: Extra Trigger Blocks Homepage
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_homeblocks() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'Home Blocks',
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) );
    register_post_type( 'homeblocks', $args );
}
function icl_theme_register_custom_homeblocks() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_homeblocks','icl_theme_register_custom_homeblocks','my_rewrite_flush_homeblocks' );
function my_rewrite_flush_homeblocks() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_homeblocks' );

?>