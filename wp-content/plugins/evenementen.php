<?php
/*
Plugin Name: Evenementen Enschede City
Description: Evenementen in the city
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_evenementen() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'Events Enschede',
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) );
    register_post_type( 'evenementen', $args );
}
function icl_theme_register_custom_evenementen() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_evenementen','icl_theme_register_custom_evenementen','my_rewrite_flush_evenementen' );
function my_rewrite_flush_evenementen() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_evenementen' );

?>