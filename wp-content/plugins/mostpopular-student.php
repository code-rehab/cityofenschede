<?php
/*
Plugin Name: Student Popular
Description: Student Most Popular
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_studentmostpopular() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'Student Most Popular',
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) );
    register_post_type( 'studentmostpopular', $args );
}
function icl_theme_register_custom_studentmostpopular() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_studentmostpopular','icl_theme_register_custom_studentmostpopular','my_rewrite_flush_studentmostpopular' );
function my_rewrite_flush_studentmostpopular() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_studentmostpopular' );

?>