<?php
/*
Plugin Name: Slider
Description: Animated Slider
Version: 1.0
Author: Squal Media
*/
?>
<?php
function codex_custom_init_slider() {
    $args = array( 
    	'orderby' => 'name', 
    	'order' => 'ASC',
    	'public' => true, 
    	'label' => 'Slider',
    	'taxonomies' => array('category'),
    	'supports' => array( 'title','author' ) );
    register_post_type( 'slider', $args );
}
function icl_theme_register_custom_slider() {
        register_taxonomy(
        'service_type',
        array('post', 'page'),
        array(
            'hierarchical' => true,
            'label' => 'Service type',
            'query_var' => true,
            'rewrite' => true
        )
    );
}
add_action( 'init', 'codex_custom_init_slider','icl_theme_register_custom_slider','my_rewrite_flush_slider' );
function my_rewrite_flush_slider() {
    flush_rewrite_rules();
}
add_action( 'after_switch_theme', 'my_rewrite_flush_slider' );

?>